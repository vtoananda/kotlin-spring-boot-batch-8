package com.techno.springbootdasar.domain.dto.request

import com.fasterxml.jackson.annotation.JsonProperty

data class ReqUserDto(
    @field:JsonProperty(value = "first_name")
    val firstName : String? = null,

    @field:JsonProperty("last_name")
    val lastName : String? = null,

    @field:JsonProperty("age")
    val age : Int? = null,

    @field:JsonProperty(value = "email")
    val email : String? = null,

    @field:JsonProperty(value = "password")
    val password : String? = null,

    @field:JsonProperty(value = "role")
    val role : String? = null,
)

