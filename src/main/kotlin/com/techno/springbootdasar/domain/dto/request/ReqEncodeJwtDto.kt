package com.techno.springbootdasar.domain.dto.request

import javax.validation.constraints.Email

data class ReqEncodeJwtDto(
    val id : Int = 0,
    val email: String = "",
    val password: String = ""
)
