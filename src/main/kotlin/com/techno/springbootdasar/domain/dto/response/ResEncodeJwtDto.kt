package com.techno.springbootdasar.domain.dto.response

data class ResEncodeJwtDto(
    val id : Int = 0,
    val token : String = ""
)
