package com.techno.springbootdasar.domain.dto.response

class ResEmailAndPasswordDto (
    val idUser : String? = null,
    val email : String? = null,
    val token : String? = null,

)
