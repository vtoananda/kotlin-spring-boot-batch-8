package com.techno.springbootdasar.domain.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.*


@Entity
@Table(name = "mst_user")
data class UserEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @field:Column(name = "uuid", columnDefinition = "uuid")
    val idUser : UUID? = null,

    @field:Column(name = "first_name", columnDefinition = "varchar(100)")
    val firstName: String? = null,

    @field:Column(name = "last_name", columnDefinition = "varchar(100)")
    val lastName : String? = null,

    @field:Column(name = "age", columnDefinition = "int")
    val age : Int? = null,

    @field:Column(name = "role", columnDefinition = "varchar(50)")
    val role : String? = null,

    @field:Column(name = "email", columnDefinition = "varchar")
    val email : String? = null,

    @field:Column(name = "password", columnDefinition = "varchar")
    val password: String? = null,

    @CreationTimestamp
    @field:Column(name = "dt_added", updatable = false, columnDefinition = "timestamp")
    val dtAdded : LocalDateTime? = null,

    @UpdateTimestamp
    @field:Column(name = "dt_updated", columnDefinition = "timestamp")
    val dtUpdated : LocalDateTime? = null,

    @ManyToOne
    @field:JoinColumn(name = "id_role", referencedColumnName = "id_role", columnDefinition = "uuid")
    val idRole : RoleEntity? = null
)

