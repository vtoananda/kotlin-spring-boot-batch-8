package com.techno.springbootdasar.domain.common

enum class StatusCode (val code: Boolean){
    SUCCESS(code = true),
    FAILED(code = false)
}