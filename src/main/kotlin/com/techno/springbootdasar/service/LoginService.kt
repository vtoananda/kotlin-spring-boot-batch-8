package com.techno.springbootdasar.service

import com.techno.springbootdasar.domain.dto.request.ReqEmailAndPasswordDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResEmailAndPasswordDto
import com.techno.springbootdasar.domain.dto.response.ResUserDto

interface LoginService {
    fun userLogin(reqEmailAndPasswordDto: ReqEmailAndPasswordDto) : ResBaseDto<Any>
}