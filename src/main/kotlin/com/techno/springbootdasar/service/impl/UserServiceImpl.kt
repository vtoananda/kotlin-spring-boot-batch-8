package com.techno.springbootdasar.service.impl

import com.techno.springbootdasar.domain.common.CommonVariable
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.request.ReqUserDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResUserDto
import com.techno.springbootdasar.domain.entity.UserEntity
import com.techno.springbootdasar.repository.UserRepository
import com.techno.springbootdasar.service.UserService
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

@Service
class UserServiceImpl(
    private val userRepository: UserRepository
) : UserService {
    override fun getUserAll(): ResBaseDto<ArrayList<ResUserDto>> {
        val data = userRepository.findAll()
        val response : ArrayList<ResUserDto> = ArrayList()
        data.forEach {
            response.add(
                ResUserDto(
                    uuid = it.idUser.toString(),
                    firstName = it.firstName,
                    lastName = it.lastName,
                    age = it.age,
                    email = it.email,
                    role = it.idRole?.name
                )
            )
        }
        return ResBaseDto(data = response)
    }

    override fun getUserById(idUser: String): ResBaseDto<ResUserDto> {
        val data = userRepository.getByIdUser(UUID.fromString(idUser))
            ?: return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = CommonVariable.FAILED_MESSAGE,
                data = null
            )

        val response = ResUserDto(
            uuid = data.idUser.toString(),
            firstName = data.firstName,
            lastName = data.lastName,
            age = data.age,
            email = data.email,
            role = data.idRole?.name
        )

        return ResBaseDto(data = response)
    }

    override fun insertUser(reqUserDto: ReqUserDto): ResBaseDto<ResUserDto> {
        val data = UserEntity(
            firstName = reqUserDto.firstName,
            lastName = reqUserDto.lastName,
            age = reqUserDto.age,
            email = reqUserDto.email,
            password = reqUserDto.password,
            role = reqUserDto.role
        )
        val resUser = userRepository.save(data)
        val response = ResUserDto(
            uuid = resUser.idUser.toString(),
            firstName = resUser.firstName,
            lastName = resUser.lastName,
            age = resUser.age,
            email = resUser.email,
            role = resUser.role
        )

        return ResBaseDto(data = response)
    }

    override fun updateUser(reqUserDto: ReqUserDto, idUser: String): ResBaseDto<ResUserDto> {
        val data = userRepository.findByIdUser(UUID.fromString(idUser)) ?:
        return ResBaseDto(
            outStat = StatusCode.FAILED.code,
            outMess = "Data not found",
            data = null
        )
        val newData = data.copy(
            firstName = reqUserDto.firstName,
            lastName = reqUserDto.lastName,
            age = reqUserDto.age,
            email = reqUserDto.email,
            password = reqUserDto.password,
            role = reqUserDto.role
        )
        val resUser = userRepository.save(newData)
        val response = ResUserDto(
            uuid = resUser.idUser.toString(),
            firstName = resUser.firstName,
            lastName = resUser.lastName,
            age = resUser.age,
            email = resUser.email,
            role = resUser.role
        )
        return ResBaseDto(data = response)
    }

    override fun deleteUser(idUser: String): ResBaseDto<Any> {
        userRepository.deleteByIdUser(UUID.fromString(idUser)) ?:
        return ResBaseDto(
            outStat = StatusCode.FAILED.code,
            outMess = CommonVariable.FAILED_MESSAGE,
            data = null
        )
        return ResBaseDto(data = null)
    }
}
