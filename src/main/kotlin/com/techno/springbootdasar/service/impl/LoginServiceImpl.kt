package com.techno.springbootdasar.service.impl

import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.request.ReqEmailAndPasswordDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResEmailAndPasswordDto
import com.techno.springbootdasar.repository.UserRepository
import com.techno.springbootdasar.service.LoginService
import com.techno.springbootdasar.util.JWTGenerator
import org.springframework.stereotype.Service

@Service
class LoginServiceImpl (
    private val userRepository: UserRepository
) : LoginService  {
     override fun userLogin(reqEmailAndPasswordDto: ReqEmailAndPasswordDto) : ResBaseDto<Any> {
         val data = userRepository.findByEmailAndPassword(
             reqEmailAndPasswordDto.email,
             reqEmailAndPasswordDto.password
         )?:
         return ResBaseDto(
             outStat = StatusCode.FAILED.code,
             outMess = "Data not found",
             data = null
         )

         val token = JWTGenerator().createJWT(
             data.idUser.toString(),
             reqEmailAndPasswordDto.email
         )

         val response = ResEmailAndPasswordDto(
             idUser = data.idUser.toString(),
             email = data.email,
             token = token

         )

         return ResBaseDto(data = response)
     }
}