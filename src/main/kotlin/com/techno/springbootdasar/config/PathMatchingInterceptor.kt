package com.techno.springbootdasar.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
class PathMatchingInterceptor (
    val requestInterceptor:RequestInterceptor,
    val authInterceptor: AuthInterceptor
) : WebMvcConfigurer {
    override fun addInterceptors(registry : InterceptorRegistry){
        registry.addInterceptor(requestInterceptor).excludePathPatterns(
            "v1/api/jwt/encode",
            "/swagger-ui/**",
            "/v2/api-docs",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/webjars/**"
        )
        registry.addInterceptor(authInterceptor).excludePathPatterns(
            "v1/api/account/login",
            "/swagger-ui/**",
            "/v2/api-docs",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/webjars/**"
        )

    }
}