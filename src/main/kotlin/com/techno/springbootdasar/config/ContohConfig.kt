package com.techno.springbootdasar.config

import com.techno.springbootdasar.service.LogicService
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class ContohConfig(
    val logicService: LogicService
)
{
    @Bean
    fun getOddOrEvent(){
        val result = logicService.oddOrEven(5)
        println("Number result is $result")
    }

}