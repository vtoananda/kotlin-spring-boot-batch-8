package com.techno.springbootdasar.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.repository.UserRepository
import com.techno.springbootdasar.util.JWTGenerator
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AuthInterceptor (
    private val userRepository: UserRepository
) : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val tokenRequest = request.getHeader("Token")
        val decodeToken = JWTGenerator().decodeJWT(tokenRequest)
        val findUser = userRepository.findByIdUser(UUID.fromString(decodeToken.id))
        if (findUser == null){
            val body:ResBaseDto<String> = ResBaseDto(outStat = StatusCode.FAILED.code, outMess = "Token Not Valid", data = null)
            internalServerError(body,response)
            return false
        }

        return super.preHandle(request, response, handler)
    }

    fun internalServerError(body : ResBaseDto<String>, response : HttpServletResponse) : HttpServletResponse{
        response.status = HttpStatus.FORBIDDEN.value()
        response.contentType = "application/json"
        response.writer.write(convertObjectToJson(body))

        return response

    }
    fun convertObjectToJson(dto : ResBaseDto<String>): String? {
        return ObjectMapper().writeValueAsString(dto)
    }
}