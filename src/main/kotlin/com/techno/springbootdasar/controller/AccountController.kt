package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqUserDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResUserDto
import com.techno.springbootdasar.service.UserService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.annotation.Resource

@RestController
@RequestMapping("/v1/api/account")
class AccountController (
    private val userService: UserService
) {
    @GetMapping
    fun getAllUser() : ResponseEntity<ResBaseDto<ArrayList<ResUserDto>>>{
        val response = userService.getUserAll()
        return  ResponseEntity.ok().body(response)
    }

    @GetMapping("/{idUser}")
    fun getByUseById(@PathVariable("idUser") idUser : String) : ResponseEntity<ResBaseDto<ResUserDto>>{
        val response = userService.getUserById(idUser)
        return  ResponseEntity.ok().body(response)
    }

    @PostMapping
    fun insertUser(@RequestBody reqUserDto: ReqUserDto) : ResponseEntity<ResBaseDto<ResUserDto>>{
        val response = userService.insertUser(reqUserDto)
        return ResponseEntity.ok().body(response)
    }

    @PutMapping("/{idUser}")
    fun updateUser(
        @RequestBody reqUserDto: ReqUserDto,
        @PathVariable("idUser") idUser: String
    ): ResponseEntity<ResBaseDto<ResUserDto>>{
        val response = userService.updateUser(
            reqUserDto,
            idUser
        )
        return ResponseEntity.ok().body(response)
    }

    @DeleteMapping("/{idUser}")
    fun deleteUser(@PathVariable("idUser") idUser: String): ResponseEntity<ResBaseDto<Any>> {
        val response = userService.deleteUser(idUser)
        return ResponseEntity.ok().body(response)
    }



}