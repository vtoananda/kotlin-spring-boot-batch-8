package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqDataDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/v1/api/example/validation")
class ValidationController {

    @PostMapping
    fun example(@Valid @RequestBody request: ReqDataDto): ResponseEntity<Any>{
        return ResponseEntity.ok("Success")
    }
}
