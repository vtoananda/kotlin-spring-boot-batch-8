package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqEmailAndPasswordDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResEmailAndPasswordDto
import com.techno.springbootdasar.service.LoginService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/api/account/login")
class LoginController (
    private val loginService: LoginService
){
    @PostMapping
    fun loginUser(@RequestBody reqEmailAndPasswordDto:ReqEmailAndPasswordDto) : ResponseEntity<ResBaseDto<Any>>{
        val response = loginService.userLogin(reqEmailAndPasswordDto)
        return if (response.outStat)
            ResponseEntity.ok().body(response)
        else
            ResponseEntity.badRequest().body(response)
    }
}