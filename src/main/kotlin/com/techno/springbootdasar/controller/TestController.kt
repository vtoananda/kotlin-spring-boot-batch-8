package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqUserDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResUserDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/api")
class TestController {
    val firstName: String = "Bayu"
    val lastName : String = "Krismanto"

    @GetMapping("/test")
    fun testGetMapping(): ResponseEntity<Any>{
        val response: LinkedHashMap<String, String> = LinkedHashMap()
        response["firstName"] = firstName
        response["lastName"] = lastName
        return ResponseEntity.ok(response)
    }

    @GetMapping("/user")
    fun getName(@RequestParam("age") age : String): ResponseEntity<Any>{
        val response: LinkedHashMap<String, String> = LinkedHashMap()
        response["firstName"] = firstName
        response["lastName"] = lastName
        response["age"] = age
        return ResponseEntity.ok(response)
    }

    @GetMapping("/user/{age}")
    fun getNameByPath(@PathVariable("age")  age : String): ResponseEntity<Any>{
        val response: LinkedHashMap<String, String> = LinkedHashMap()
        response["firstName"] = firstName
        response["lastName"] = lastName
        response["age"] = age
        return ResponseEntity.ok(response)
    }

    @GetMapping("/user/dto")
    fun getUser(@RequestParam("age") age : Int): ResponseEntity<ResUserDto>{
        val response = ResUserDto(
            firstName = this.firstName,
            lastName = this.lastName,
            age = age
        )

        return ResponseEntity.ok().body(response)
    }

    @PostMapping("/user")
    fun postUser(@RequestBody  reqUserDto: ReqUserDto) : ResponseEntity<ResBaseDto<ResUserDto>>{
        val data = ResUserDto(
            firstName = reqUserDto.firstName,
            lastName = reqUserDto.lastName,
            age = reqUserDto.age
        )

        val response = ResBaseDto(data = data)
        return ResponseEntity.ok().body(response)
    }

}
