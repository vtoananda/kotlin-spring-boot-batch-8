package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqDecodeJwtDto
import com.techno.springbootdasar.domain.dto.request.ReqEncodeJwtDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResDecodeJwtDto
import com.techno.springbootdasar.domain.dto.response.ResEncodeJwtDto
import com.techno.springbootdasar.util.JWTGenerator
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/api/jwt")
class JWTController {

    @PostMapping("/encode")
    fun encodeJwt(@RequestBody reqEncodeJwtDto: ReqEncodeJwtDto): ResponseEntity<ResBaseDto<ResEncodeJwtDto>> {
        val token = JWTGenerator().createJWT(
            reqEncodeJwtDto.id.toString(),
            reqEncodeJwtDto.email
        )

        val response = ResEncodeJwtDto(
            id = reqEncodeJwtDto.id,
            token = token
        )

        val resBase = ResBaseDto(data = response)

        return ResponseEntity.ok().body(resBase)
    }

    @PostMapping("/decode")
    fun decodeJwt(@RequestBody reqDecodeJwtDto: ReqDecodeJwtDto): ResponseEntity<ResBaseDto<ResDecodeJwtDto>> {
        val claims = JWTGenerator().decodeJWT(reqDecodeJwtDto.token)
        val response = ResDecodeJwtDto(
            id = claims.id.toInt(),
            email = claims.subject
        )
        val resBase = ResBaseDto(data = response)
        return ResponseEntity.ok().body(resBase)
    }
}
