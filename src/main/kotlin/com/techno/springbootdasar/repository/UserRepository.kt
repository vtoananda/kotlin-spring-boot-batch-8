package com.techno.springbootdasar.repository

import com.techno.springbootdasar.domain.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import java.util.UUID
import javax.transaction.Transactional

interface UserRepository : JpaRepository<UserEntity, String>{

    // Query by method
    fun findByIdUser(idUser : UUID) : UserEntity?

    fun findUserEntityByIdUser(idUser: UUID) : UserEntity?
    fun findByEmailAndPassword(email: String?, password: String?) : UserEntity?

//     query raw
    @Query("SELECT a FROM UserEntity a WHERE idUser = :idUser")
    fun getByIdUser(idUser: UUID) : UserEntity?

    // query raw native
    @Query("SELECT * FROM mst_user mu WHERE mu.uuid = :idUser", nativeQuery = true)
    fun getByIdUserNative(idUser: UUID) : List<String>?

    @Modifying
    @Transactional
    fun deleteByIdUser(idUser: UUID) : Int?
}
